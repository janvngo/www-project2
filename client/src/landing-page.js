import { LitElement, html, css, property } from 'lit-element';

export class LandingPage extends LitElement {

    static get properties(){
        return {
            location: Object
        };
    }

constructor(){
    super();
    
}

    static styles = css`
            html
            {
                margin: 0;
                padding: 0;
                color: #aaaaaa;
                background-color: #333333;
                font: normal 12px "Trebuchet MS", "Lucida Grande", Verdana, Georgia, Sans-Serif;
            }

            /* Links */

            a:link { text-decoration: none; color: #7799bb; }

            a:visited { text-decoration: none; color: #557799; }

            a:hover, a:active { color: #dd5555; }

            /* Html Elements */

            h1, h2, h3, h4, h5, h6 { font-weight: normal; margin: 0; padding: 10px 0; color: #779955; }

            h1 { font-size: 32px; }

            h2 { font-size: 28px; }

            h3 { font-size: 24px; }

            h4 { font-size: 20px; }

            h5, h6 { font-size: 17px; }

            ul, ol, dl, blockquote, pre
            {
                color: #999999;
                margin: 10px 35px; 
                padding: 10px 30px; 
                line-height: 1.6em;
                border: 1px solid #1a1a1a;
                border-top: 10px solid #151515;
                background: #404040 url(images/navigation_hover.gif) top left repeat;
            }

            p { margin: 0; }

            ul { list-style-type: square; } 

            dt
            {
                margin: 0;
                color: #7799bb;
                font-size: 10px; 
                font-weight: bold;
                padding: 10px 0 2px; 
                text-transform: uppercase; 
            }

            dd { margin: 0; padding: 2px 0 5px; margin-bottom: 5px; border-bottom: 1px dotted #555555; }

            dd.last { border-bottom-width: 0; }

            pre { font-family: Consolas, Verdana, "Courier New"; font-size: 11px; padding: 10px 30px; }

            code { color: #779955; }

            abbr, acronym { text-decoration: none; border-bottom: 1px dotted #aaaaaa; cursor: help; }

            em { font-style: italic; }

            strong { font-weight: bold; }

            ins { color: #999999; text-decoration: none; padding-left: 20px; background: transparent url(images/information.png) center left no-repeat; }

            del { text-decoration: line-through; } 

            blockquote { text-align: right; }

            blockquote p { text-align: left; }

            blockquote cite { font-style: normal; padding-right: 10px; }

            /* Structure */

            body
            {
                margin: 0;
                float: left;
                width: 100%;
                background: black;
            }

            #header
            {
                float: left; 
                width: 100%; 
                clear: both;
                border-top: 10px solid #111111;
                background: #222222 url(images/navigation_hover.gif) top left repeat;
            }

            #navigation
            {
                float: left;
                width: 100%; 
                clear: both;
                color: #333333;
                border-bottom: 5px solid #222222;
                background: #eeeeee url(images/navigation_bg.gif) top left repeat; 
            }

            #wrapper
            {
                clear: both;
                float: left;
                width: 100%;
                margin: 10px 0; 
                background: transparent url(images/hr_dotted.gif) 60% 0 repeat-y; 
            }

            #content-wrapper
            {
                width: 60%;
                float: left;
            }

            #content
            {
                float: left;
                padding: 0 20px;
            }

            #sidebar-wrapper
            {
                width: 40%;
                float: left;
            }

            #sidebar
            {
                float: left;
                padding: 0 20px;
            }

            #footer
            {
                clear: both;
                float: left;
                width: 100%; 
                color: #888888;
                text-align: center; 
                background-color: #222222;
                border-top: 1px solid #000000;
                border-bottom: 10px solid #191919;
            }

            /* Header */

            #header h1 { padding: 20px; }

            /* Navigation */

            #navigation ul { float: left; list-style-type: none; margin: 0; padding: 0; background: none; border-width: 0; }

            #navigation li { float: left; }

            #navigation a
            {
                float: left;
                display: block;
                padding: 5px 10px; 
            }

            #navigation a:link,
            #navigation a:visited
            {
                color: #002244; 
                font-weight: bold; 
                text-transform: uppercase; 
            }

            #navigation a:hover,
            #navigation a:active
            {
                color: #7799bb;
                background: #333333 url(images/navigation_hover.gif) top left repeat;
            }

            li#lhome a:hover,
            li#lhome a:active
            {
                background-position: 0 -3px; 
            }

            li#lproducts a:hover,
            li#lproducts a:active
            {
                background-position: 0 1px; 
            }

            li#lsolutions a:hover,
            li#lsolutions a:active
            {
                background-position: 0 2px; 
            }

            li#lmysterious a:hover,
            li#lmysterious a:active
            {
                background-position: 0 -1px; 
            }

            li#labout a:hover,
            li#labout a:active
            {
                background-position: 0 -2px; 
            }

            li#lcontact a:hover,
            li#lcontact a:active
            {
                background-position: 0 -3px; 
            }

            body#babout li#labout a
            {
                color: #99bbdd;
                background: black;
            }

            /* Sidebar */

            #sidebar p
            {
                color: #999999;
                margin: 10px 35px; 
                padding: 5px 30px; 
                line-height: 1.6em;
                border: 1px solid #1a1a1a;
                border-top: 10px solid #151515;
                background: #404040 url(images/navigation_hover.gif) top left repeat;
            }

            /* Footer */

            #footer p { margin: 15px 0; }

            #footer a:link { color: #888888; font-weight: bold; border-bottom: 1px dotted #888888; } 

            #footer a:visited { color: #666666; font-weight: bold; border-bottom: 1px dotted #666666; }

            #footer a:hover, #footer a:active { color: #999999; border-bottom: 1px solid #999999; }
    `;

    

    render() {
        return html`
        <body>
            <div id="header">
            <h1>Forum</h1>
            </div>
            <div id="navigation">
                <ul>
                <li id="lhome"><a href="/">Home</a></li>
                <li id="login"><a href="/login">Login</a></li>
                <li id="register"><a href="/register">Register</a></li>
                </ul>
            </div>
            <div id="wrapper">
                <div id="content-wrapper">
                <div id="content">
                    <h4>List of contents</h4>
                    <dl>
                        <dt>Post 1</dt>
                        <dd>Here goes post 1 <em> Bla bla bla bla bla bla bla bla </em></dd>
                        <dt>Post 2</dt>
                        <dd>Here goes post 2</dd>
                        <dt>Post 3</dt>
                        <dd>Here goes post 3</dd>
                        <dt>Post 4</dt>
                        <dd>Here goes post 4</dd>
                        <dt>Post 5</dt>
                        <dd class="last"> <em> Here goes post 6 </em></dd>
                    </dl>
                    <h6>What to have here?</h6>
                    <blockquote>
                    <p>Just testing</p>
                    </blockquote>
                    <pre><code>Top rated posts maybe?</code></pre>
                </div>
                </div>
                <div id="sidebar-wrapper">
                <div id="sidebar">
                    <h3>Unordered list for what?</h3>
                    <ul>
                    <li>Members?</li>
                    <li>Likes?</li>
                    <li>Comments?</li>
                    <li>?????</li>
                    </ul>
                    <h3>Ordered list for something?</h3>
                    <ol>
                    <li>I dont know</li>
                    <li>What to add</li>
                    <li>At this point</li>
                    <li>Anymore</li>
                    </ol>
                    <h3>Stats</h3>
                    <p> Potential member stats here? </p>
                </div>
                </div>
            </div>
        </body>

        `;
            
    }

}

    
customElements.define('landing-page', LandingPage);