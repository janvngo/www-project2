import {Router} from '@vaadin/router';
import '/src/landing-page.js'
import '/src/view-login.js'
import '/src/view-register.js'


const landing = document.getElementById('landing');
const router = new Router (landing);

router.setRoutes([{path: '/',         component: 'landing-page'},
                  {path: '/register', component: 'view-register'  },
                  {path: '/login',    component: 'view-login'     },
                  /*{path: '/profile',  component: './view-profile.js',
                   children : [
                    {path: '/posts',    component: 'view-posts'   },
                    {path: '/comments', component: 'view-comments'}
                   ]}*/
                  //{path: '(.*)',      component: 'noView-element'} //Page not found.
                  //Default routing perhaps aswell.
                ]);