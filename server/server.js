"use strict";

import express from 'express';
import path from 'path';
import mysql from 'mysql';

const app = express();
const PORT = 8081;
const router = express.Router(); //vaadin router

import url from 'url';
import expValidator from 'express-validator';
import { request } from 'http';
import crypto from 'crypto';


app.use(express.urlencoded({
    extended: false
}));

app.use(express.json());


app.listen(PORT, () => {
    console.log('Running...');
})

app.use(express.static(path.resolve() + '/server'));

var db = mysql.createConnection({
    host: "db",
    user: "admin",
    password: "password",
    database: 'prog2053-proj'
});

db.connect(function(err) {
    if (err) {
        throw err;
    }
    console.log("Connected!");
});

app.use(function(req, res, next) {

    // Website you wish to allow to connect
    res.setHeader('Access-Control-Allow-Origin', 'http://localhost:8080');

    // Request methods you wish to allow
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');

    // Request headers you wish to allow
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');

    // Set to true if you need the website to include cookies in the requests sent
    // to the API (e.g. in case you use sessions)
    res.setHeader('Access-Control-Allow-Credentials', true);

    

    // Pass to next layer of middleware
    next();
});




// Renders the register page on request

app.get('/register', (req, res) => {
    res.render("register");
});

// Renders the login page on request
app.get('/login', (req, res) => {
    res.render("login");
});

// Code for registering the user to database
app.post('/register', (req, res) => {

    let key = 'test';
    var hash = crypto.pbkdf2Sync(req.body.password, key, 1000, 64, `sha512`).toString(`hex`);;

    var users = {
        "email": req.body.email,
        "password": hash

    }
    db.query('SELECT email FROM users WHERE email = ?', [users.email], function(err, results) {
        if (results.length > 0) { // Checks if email is already registered
            res.status(401).send({
                "success": "Error: this email is already registered"
            })
        } else {
            db.query('INSERT INTO users SET ?', users, function(error, results, fields) {

                if (error) {
                    res.json({
                        status: false,
                        message: 'Fail: error with query'
                    })
                } else {
                    res.send({
                        status: true,
                        data: results,
                        message: 'Success: User has been registered successfully'
                    })
                }
            });
        }
    });


});

app.get('/getUsers', function(req, res) {
    db.query('SELECT * FROM users', function(err, result) {
        if (err) {
            res.status(400).send('Error with database operation');
        } else {
            res.writeHead(200, { 'Content-Type': 'application/json' });
            res.end(JSON.stringify(result));
            console.log("Result: " + res);
        };
    });
});




/* app.get('/login', (req, res) => {

    var msg = "";
    var sqlErr = "";
    var user = {
        "email": req.body.email,
        "password": req.body.password
    }
    try { // To handle potential errors
        db.query(`SELECT email, password FROM users WHERE email='${user.email}', password='${user.password}'`, function(err, result) {
            if (err) {
                throw err.message;
            }
            msg = "Login succesful."
            res.send(msg);

            //Have to res send a auth token aswell.
        });

    } catch (err) { //If error: user not found in DB
        sqlErr = err;
        res.status(404).send("User non-existent: " + sqlErr);
    }

}); */




// Code for logging in, checks existing information in database 

app.post('/login', (req, res) => {

    
    let key = 'test';
    var hash = crypto.pbkdf2Sync(req.body.password, key,
        1000, 64, `sha512`).toString(`hex`);;


    var user = {
        "email": req.body.email,
        "password": hash
    }
    db.query('SELECT * FROM users WHERE email = ?', [user.email, user.password], async function(error, results, fields) {
        if (error) {
            res.status(400).send({
                "Fail": "An error has occured"
            })

        } else {
            if (results.length > 0) {

                res.status(200).send({
                        "Success": "Logged in successfully"
                    })
                    // redirect*()....
            } else {
                res.status(401).send({
                    "Success": "Error: credentials are not correct"
                });
            }
        }
    });
});